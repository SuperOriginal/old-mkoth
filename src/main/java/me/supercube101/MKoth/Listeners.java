package me.supercube101.MKoth;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;

import java.util.List;
import java.util.Random;

/**
 * Created by Aaron.
 */
public class Listeners implements Listener{

    @EventHandler
    public void onCaptureHandle(PlayerMoveEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        Player p  = e.getPlayer();
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){

                if(k.isOnHill(e.getPlayer().getLocation()) && !k.getPlayers().contains(e.getPlayer().getName())) k.getPlayers().add(e.getPlayer().getName());
                if(!k.isOnHill(e.getPlayer().getLocation()) && k.getPlayers().contains(e.getPlayer().getName())) k.getPlayers().remove(e.getPlayer().getName());
                if(k.isOnHill(e.getPlayer().getLocation()) && e.getPlayer().isDead() && k.getPlayers().contains(e.getPlayer().getName())) k.getPlayers().remove(e.getPlayer().getName());

                //First, let's check if  player is the first capper
                if(k.isOnHill(p.getLocation()) && k.getCapturer() == null) k.setCapturer(p);
                //-------

                //Check if player is capper, but is no longer in the region.
                if(k.getCapturer() != null && k.getCapturer().equals(p) && !k.isOnHill(p.getLocation())){
                    //Was player hit off by another player?
                    if(k.getCapturer().getLastDamageCause() != null && k.getCapturer().getLastDamageCause().getEntity() != null && k.getCapturer().getLastDamageCause().getEntity() instanceof Player && k.isOnHill(k.getCapturer().getLastDamageCause().getEntity().getLocation())){
                        k.setCapturer((Player)k.getCapturer().getLastDamageCause().getEntity()); //Make that player the new capper.

                    }else{ //So apparently the capper is just retarded and walked out, let's set a random player to become the next capper. If nobody is there, it will be null.
                        k.setCapturer(pickRandom(k));
                    }
                }
                //------

           }
        }
    }

    @EventHandler
    public void onEat(PlayerItemConsumeEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        if(!e.getItem().getType().equals(Material.GOLDEN_APPLE)) return;
        if(e.getItem().getData().getData() != 1) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){
                if(k.isInArena(e.getPlayer().getLocation())){
                    if(k.getGapples().get(e.getPlayer().getName()) != null && k.getGapples().get(e.getPlayer().getName()) + 1 > 5){
                        e.setCancelled(true);
                        e.getPlayer().sendMessage(ChatColor.RED + "You may only eat 5 god apples per koth!");
                        return;
                    }
                    if(!k.getGapples().containsKey(e.getPlayer().getName())) k.getGapples().put(e.getPlayer().getName(), 1);
                    else k.getGapples().put(e.getPlayer().getName(), k.getGapples().get(e.getPlayer().getName()) + 1);
                }
            }
        }
    }

    @EventHandler
    public void onTP(PlayerTeleportEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){
                if(k.isInArena(e.getTo())){
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(ChatColor.RED + "Error: You cannot tp into a running koth arena!");
                }
            }
        }
    }

    @EventHandler
    public void deathHandler(PlayerMoveEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){
                if(k.getDead().contains(e.getPlayer().getName())){
                    if(k.isInArena(e.getTo())){
                        e.getPlayer().teleport(e.getFrom());
                        e.getPlayer().sendMessage(ChatColor.RED + "You have died! You may not reenter!");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDMG(EntityDamageByEntityEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()) {
            if (k.isPlaying()) {
                if (e.getEntity() instanceof Player && e.getDamager() instanceof Arrow) {
                    if (((Arrow) e.getDamager()).getShooter() instanceof Player) {
                        Player p = (Player) ((Arrow) e.getDamager()).getShooter();
                        if (k.getDead().contains(p.getName()) && k.isInArena(e.getEntity().getLocation())){
                            e.setCancelled(true);
                            p.sendMessage(ChatColor.RED + "You cannot interfere with a koth you have died in!");
                        }
                    }
                }
                
                //-------------------------------
                if (e.getEntity() instanceof Player && e.getDamager() instanceof Snowball) {
                    if (((Snowball) e.getDamager()).getShooter() instanceof Player) {
                        Player p = (Player) ((Snowball) e.getDamager()).getShooter();
                        if (k.getDead().contains(p.getName()) && k.isInArena(e.getEntity().getLocation())){
                            e.setCancelled(true);
                            p.sendMessage(ChatColor.RED + "You cannot interfere with a koth you have died in!");
                        }
                    }
                }
                //--------------------------------
                if (e.getEntity() instanceof Player && e.getDamager() instanceof Egg) {
                    if (((Egg) e.getDamager()).getShooter() instanceof Player) {
                        Player p = (Player) ((Egg) e.getDamager()).getShooter();
                        if (k.getDead().contains(p.getName()) && k.isInArena(e.getEntity().getLocation())){
                            e.setCancelled(true);
                            p.sendMessage(ChatColor.RED + "You cannot interfere with a koth you have died in!");
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        if(e.getPlayer().hasPermission("mkoth.admin")) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){
                if(k.isInArena(e.getPlayer().getLocation())){
                    for(String s : MKoth.blockedcmds){
                        if(e.getMessage().startsWith(s)){
                            e.setCancelled(true);
                            e.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to use that command in KOTH!");
                        }
                    }
                }
            }
        }
    }
    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()) {
                if (k.getCapturer() != null && k.getCapturer().getName().equals(e.getPlayer().getName())) {
                    if (k.getCapturer().getLastDamageCause() != null && k.getCapturer().getLastDamageCause().getEntity() != null && k.getCapturer().getLastDamageCause().getEntity() instanceof Player && k.isOnHill(k.getCapturer().getLastDamageCause().getEntity().getLocation())) {
                        k.setCapturer((Player) k.getCapturer().getLastDamageCause().getEntity()); //Make that player the new capper.
                    } else {
                        k.setCapturer(pickRandom(k));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onKick(PlayerKickEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()) {
                if (k.getCapturer().getName().equals(e.getPlayer().getName())) {
                    if (k.getCapturer().getLastDamageCause() != null && k.getCapturer().getLastDamageCause().getEntity() != null && k.getCapturer().getLastDamageCause().getEntity() instanceof Player && k.isOnHill(k.getCapturer().getLastDamageCause().getEntity().getLocation())) {
                        k.setCapturer((Player) k.getCapturer().getLastDamageCause().getEntity()); //Make that player the new capper.
                    } else {
                        k.setCapturer(pickRandom(k));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        if(MKoth.i.nothingIsPlaying()) return;
        for(Koth k : MKoth.i.getKoths()){
            if(k.isPlaying()){
                //Was he capping?
                if(k.getCapturer() != null && k.getCapturer().equals(e.getEntity())){
                    if(e.getEntity().getKiller() == null) k.setCapturer(pickRandom(k)); //Nobody killed
                    else k.setCapturer(e.getEntity().getKiller()); //Someone killed
                }
                //-----
                if(k.isInArena(e.getEntity().getLocation())){
                    k.getDead().add(e.getEntity().getName());
                    if(e.getEntity().getKiller() != null){
                        if(k.getKills().containsKey(e.getEntity().getKiller().getName())) k.getKills().put(e.getEntity().getName(), k.getKills().get(e.getEntity().getKiller().getName()));
                        else k.getKills().put(e.getEntity().getKiller().getName(), 1);
                    }
                }
            }
        }
    }

    public Player pickRandom(List<String> string){
        Random ran = new Random();
        if(string.isEmpty()) return null;
        return Bukkit.getServer().getPlayer(string.get(ran.nextInt(string.size())));
    }

    public Player pickRandom(Koth k){
        Random ran = new Random();
        if(k.getPlayers().isEmpty()) return null;
        return Bukkit.getServer().getPlayer(k.getPlayers().get(ran.nextInt(k.getPlayers().size())));
    }
}
