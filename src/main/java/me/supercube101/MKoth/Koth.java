package me.supercube101.MKoth;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.supercube101.MKoth.TimerManagement.Cooldown;
import net.minecraft.util.org.apache.commons.lang3.text.WordUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Aaron.
 */
public class Koth {

    private String name;
    private World world;
    private String arenaid;
    private String captureid;
    private Player capturer = null;
    public boolean playing;
    private HashMap<String, Double> scores;
    private ArrayList<String> players;
    private ArrayList<String> dead;
    private HashMap<String, Integer> kills;
    public ArrayList<Block> air;
    private HashMap<String, Integer> gapples;
    public Set<Chunk> occupies;
    public Set<Chunk> visable;
    public Set<Player> glassvisable;



    public Koth(String name, World world, String arena, String capture){
        this.name = WordUtils.capitalize(name);
        this.world = world;
        this.arenaid = arena;
        this.captureid = capture;
        this.playing = false;
        kills = new HashMap<String, Integer>();
        scores = new HashMap<String, Double>();
        players = new ArrayList<String>();
        dead = new ArrayList<String>();
        air = new ArrayList<Block>();
        gapples = new HashMap<String, Integer>();
        occupies = new HashSet<Chunk>();
        visable = new HashSet<Chunk>();
        glassvisable = new HashSet<Player>();
        propegateChunks();
        ProtectedRegion r = MKoth.i.getWorldGuardInstance().getRegionManager(getWorld()).getRegion(getArenaid());
        if(r instanceof ProtectedCuboidRegion){
            ProtectedCuboidRegion protectedCuboidRegion = (ProtectedCuboidRegion) r;
            BlockVector l = protectedCuboidRegion.getMinimumPoint();
            BlockVector h = protectedCuboidRegion.getMaximumPoint();
            Location cache = new Location(getWorld(), 0,0,0);
            for(int x = l.getBlockX(); x<=h.getBlockX(); x++){
                cache.setX(x);
                for(int y = l.getBlockY(); y<=h.getBlockY(); y++){
                    cache.setY(y);
                    for(int z = l.getBlockZ(); z<=h.getBlockZ(); z++){
                        cache.setZ(z);
                        if(cache.getBlock().getType() == null || cache.getBlock().getType().equals(Material.AIR)){
                            for(BlockFace face : BlockFace.values()){
                                if(!isInArena(cache.getBlock().getRelative(face).getLocation())) air.add(cache.getBlock());
                            }
                        }
                    }
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public World getWorld() {
        return world;
    }

    public String getArenaid() {
        return arenaid;
    }

    public String getCaptureid() {
        return captureid;
    }

    public boolean isPlaying(){
        return this.playing;
    }

    public void setPlaying(boolean playings){
        playing = playings;
    }

    public Player getCapturer() {
        return capturer;
    }

    public void setCapturer(Player capturer) {
        if(this.capturer != null && Cooldown.isCooling(this.capturer.getName(), name) && Cooldown.getRemaining(this.capturer.getName(), name) < 240){
            if(capturer == null){
                //Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.BLUE + name + " is no longer being captured.");
            }else{
                Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.BLUE + name + " is being captured. (5:00)");
            }
        }
        if(this.capturer != null){
            this.capturer.sendMessage(ChatColor.BLUE + "You are no longer capturing " + name + "!");
            Cooldown.removeCooldown(this.capturer.getName(), name);
        }
        this.capturer = capturer;
        if(capturer != null) {
            capturer.sendMessage(ChatColor.BLUE + "You are now capturing " + name + "!");
            Cooldown.add(capturer.getName(), name, 300, System.currentTimeMillis());
            //300
        }

    }

    public HashMap<String, Integer> getGapples() {
        return gapples;
    }

    public HashMap<String, Double> getScores() {
        return scores;
    }

    public void reset(){
        MKoth.i.updatePlayerStatsInDB(dead,kills,scores);
        capturer = null;
        playing = false;
        scores.clear();
        players.clear();
        dead.clear();
       gapples.clear();
        MKoth.i.localwins.clear();
       Cooldown.removeCooldown(name, "run");
       MKoth.i.retrieveTopStats();
    }

    public HashMap<String, Integer> getKills() {
        return kills;
    }

    public ArrayList<String> getPlayers() {
        return players;
    }

    public ArrayList<String> getDead() {
        return dead;
    }

    public boolean isOnHill(Location p){
        ApplicableRegionSet set = MKoth.i.getWorldGuardInstance().getRegionManager(p.getWorld()).getApplicableRegions(p);
        for(ProtectedRegion region : set){
            if(region.getId().equalsIgnoreCase(captureid)) return true;
        }
        return false;
    }

    public boolean isInArena(Location p){
        ApplicableRegionSet set = MKoth.i.getWorldGuardInstance().getRegionManager(p.getWorld()).getApplicableRegions(p);
        for(ProtectedRegion region : set){
            if(region.getId().equalsIgnoreCase(arenaid)) return true;
        }
        return false;
    }

    public void propegateChunks(){
        World w = world;
        ProtectedRegion r = MKoth.i.getWorldGuardInstance().getRegionManager(w).getRegion(arenaid);
        if (r instanceof ProtectedCuboidRegion) {
            ProtectedCuboidRegion cubeRegion = (ProtectedCuboidRegion) r;
            Chunk high = w.getChunkAt(fromWGVector(cubeRegion.getMaximumPoint(),w));
            Chunk low = w.getChunkAt(fromWGVector(cubeRegion.getMinimumPoint(),w));
            for(int x = low.getX(); x <= high.getX(); x++){
                for(int z = low.getZ(); z <= high.getZ(); z++){
                    occupies.add(w.getChunkAt(x,z));
                }
            }
        }

        Integer dist = 3;

        for(Chunk k : occupies){
            for(int x = k.getX()-dist; x<=k.getX()+dist; x++){
                for(int z = k.getZ()-dist; z<=k.getZ()+dist; z++){
                    if(distance(k.getX(),k.getZ(),x,z) <= dist){
                        visable.add(w.getChunkAt(x,z));
                    }
                }
            }
        }
    }

    private Location fromWGVector(BlockVector v, World w){
        return new Location(w,v.getX(),v.getY(),v.getZ());
    }

    private Double distance(int x1, int z1, int x2, int z2){
        return Math.sqrt(((x2-x1)*(x2-x1))+((z2-z1)*(z2-z1)));
    }

    public boolean processPlayer(Player p){
        if(glassvisable.contains(p)){

            if(!visable.contains(p.getLocation().getChunk()) && !occupies.contains(p.getLocation().getChunk())) {
                glassvisable.remove(p);
                Bukkit.broadcastMessage("FALSE");
                return false;
            }
        }
        else{
            if(visable.contains(p.getLocation().getChunk()) || occupies.contains(p.getLocation().getChunk())){
                glassvisable.add(p);
                Bukkit.broadcastMessage("TRUE");
                return true;
            }
        }
        return false;
    }




}
