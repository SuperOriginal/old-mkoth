package me.supercube101.MKoth;

import com.mongodb.*;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import me.supercube101.MKoth.TimerManagement.Cooldown;
import me.supercube101.MKoth.TimerManagement.GlassTask;
import me.supercube101.MKoth.TimerManagement.Tasks;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by Aaron.
 */
public class MKoth extends JavaPlugin {

    public static MKoth i;
    private ArrayList<Koth> koths;
    private File kothfile;
    private YamlConfiguration kothconfig;
    private Tasks stask;
    public MongoClient client;
    public DB db;
    public DBCollection coll;
    public HashMap<String, Integer> localkills, localdeaths, localwins;
    public HashMap<String, Integer> localscore;
    public static ArrayList<String> blockedcmds;

    public void onEnable() {
        i = this;
        blockedcmds = new ArrayList<String>();
        try {
            client = new MongoClient("localhost");
        } catch (UnknownHostException e) {
            getLogger().severe("Unable to establish connection to database.");
        }
        db = client.getDB("mkoth");
        coll = db.getCollection("playerstats");
        handleBlockedCmds();

        koths = new ArrayList<Koth>();
        getCommand("koth").setExecutor(new KothCmdHandler());
        //getCommand("stats").setExecutor(new StatusCmdHandler());
        handleKothFile();
        kothconfig = YamlConfiguration.loadConfiguration(kothfile);
        deserializeKoths();
        getServer().getPluginManager().registerEvents(new Listeners(), this);
        stask = new Tasks();
        localdeaths = new HashMap<String, Integer>();
        localkills = new HashMap<String, Integer>();
        localscore = new HashMap<String, Integer>();
        localwins = new HashMap<String, Integer>();

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                Cooldown.handleCooldowns();
                stask.handleScores();
            }
        }, 1, 1);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                if (Cooldown.sent) Cooldown.sent = false;
            }
        }, 20, 20);

        retrieveTopStats();
    }


    public void handleKothFile() {
        kothfile = new File(getDataFolder(), "kothdata.yml");
        if (!kothfile.exists()) try {
            kothfile.createNewFile();
        } catch (Exception e) {
            getLogger().severe("Failed to load kothdata.yml");
        }
    }

    public YamlConfiguration getKothData() {
        return kothconfig;
    }

    public void saveKothFile() {
        try {
            getKothData().save(kothfile);
        } catch (Exception e) {
            getLogger().severe("unable to save kothdata.yml!");
        }
    }

    public WorldGuardPlugin getWorldGuardInstance() {
        return (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
    }

    public ArrayList<Koth> getKoths() {
        return koths;
    }

    public void deserializeKoths() {
        if (getKothData().getKeys(false).isEmpty()) return;
        for (String key : getKothData().getKeys(false)) {
            try {
                ConfigurationSection section = getKothData().getConfigurationSection(key);
                String arenaid = section.getString("arenaid");
                String captureid = section.getString("captureid");
                World world = getServer().getWorld(section.getString("world"));
                getKoths().add(new Koth(key, world, arenaid, captureid));
            } catch (Exception e) {
                e.printStackTrace();
                getLogger().severe("ERROR LOADING KOTH " + key + ". Did you modify the data file?");
            }

        }
    }

    public boolean nothingIsPlaying() {
        for (Koth koth : getKoths()) {
            if (koth.playing) return false;
        }
        return true;
    }

    public Koth getKoth(Player p) {
        for (Koth koth : MKoth.i.getKoths()) {
            if (koth.getPlayers().contains(p.getName()) && koth.isPlaying()) return koth;
        }
        return null;
    }

    public Koth getKothByArena(Player p) {
        for (Koth koth : MKoth.i.getKoths()) {
            if (koth.isInArena(p.getLocation()) && koth.isPlaying()) return koth;
        }
        return null;
    }

    public Koth getKoth(String s) {
        for (Koth koth : MKoth.i.getKoths()) {
            if (koth.getName().equalsIgnoreCase(s)) return koth;
        }
        return null;
    }

    public void handleBlockedCmds(){
        blockedcmds.add("enderchest");
        blockedcmds.add("echest");
        blockedcmds.add("enderedit");
        blockedcmds.add("endersee");
        blockedcmds.add("eenderchest");
        blockedcmds.add("eechest");
        blockedcmds.add("eenderedit");
        blockedcmds.add("eendersee");
        blockedcmds.add("auc");
        blockedcmds.add("arena");
        blockedcmds.add("eec");
        blockedcmds.add("vault");
    }

    public void updatePlayerStatsInDB(List<String> dead, HashMap<String, Integer> kills, HashMap<String, Double> score) {
            HashSet<String> all = new HashSet<String>();
            for (String s : dead) all.add(s);
            for (String s : kills.keySet()) all.add(s);
            for (String s : score.keySet()) all.add(s);
            for (String s : MKoth.i.localwins.keySet()){
                if(s != null) all.add(s);
            }
            if (all.isEmpty()) return;
            for (String s : all) {
                    BasicDBObject query = new BasicDBObject("uuid", Bukkit.getServer().getOfflinePlayer(s).getUniqueId().toString());
                    DBCursor cursor = coll.find(query);
                    if (cursor.size() == 0) { //Not found in DB
                        BasicDBObject obj = new BasicDBObject();
                        obj.append("uuid", Bukkit.getOfflinePlayer(s).getUniqueId().toString());
                        obj.append("kills", 0);
                        obj.append("deaths", 0);
                        obj.append("score", 0);
                        obj.append("wins", 0);
                        coll.insert(obj);
                    }
                    int killamt = 0;
                    int deathamt = 0;
                    int winamt = 0;
                    double scoreamt = 0;
                    while (cursor.hasNext()) {
                        DBObject stats = cursor.next();
                        killamt = ((Number) stats.get("kills")).intValue();
                        deathamt = ((Number) stats.get("deaths")).intValue();
                        scoreamt = ((Number) stats.get("score")).intValue();

                    }
                    if (dead.contains(s)) deathamt = deathamt + 1;
                    if (kills.containsKey(s)) killamt = killamt + kills.get(s);
                    if (score.containsKey(s)) scoreamt = scoreamt + score.get(s);
                    if (MKoth.i.localwins.containsKey(s)) winamt = MKoth.i.localwins.get(s);

                    BasicDBObject toUpdateKills = new BasicDBObject();
                    BasicDBObject toUpdateDeaths = new BasicDBObject();
                    BasicDBObject toUpdateScore = new BasicDBObject();
                    BasicDBObject toUpdateWins = new BasicDBObject();
                    toUpdateKills.append("$set", new BasicDBObject("kills", killamt));
                    toUpdateDeaths.append("$set", new BasicDBObject("deaths", deathamt));
                    toUpdateScore.append("$set", new BasicDBObject("score", scoreamt));
                    toUpdateWins.append("$set", new BasicDBObject("wins", winamt));

                    coll.update(query, toUpdateKills);
                    coll.update(query, toUpdateScore);
                    coll.update(query, toUpdateDeaths);
                    coll.update(query, toUpdateWins);



            }
    }

    public void retrieveTopStats() {
        DBCursor cursor = coll.find();
        while (cursor.hasNext()) {
            DBObject stat = cursor.next();
            String name = Bukkit.getOfflinePlayer(UUID.fromString((String) stat.get("uuid"))).getName();
            int deaths = ((Number) stat.get("deaths")).intValue();
            int kills = ((Number) stat.get("kills")).intValue();
            int score = ((Number) stat.get("score")).intValue();
            int wins = ((Number) stat.get("wins")).intValue();
            if (kills > 0) localkills.put(name, kills);
            if (deaths > 0) localdeaths.put(name, deaths);
            if (score > 0) localscore.put(name, score);
            if (wins > 0) localwins.put(name, wins);
        }

    }

    public static TreeMap<String, Integer> sort(final HashMap<String,Integer> map) {


    List<String> list = new ArrayList<String>(map.keySet());

    Comparator<String> cmp = new Comparator<String>() {
        @Override
        public int compare(String a1, String a2) {
            Integer timesPlayed1 = map.get(a1);
            Integer timesPlayed2 = map.get(a2);
            return timesPlayed1.compareTo(timesPlayed2);
        }
    };
    Collections.sort(list,Collections.reverseOrder(cmp));
    TreeMap<String, Integer> sorted = new TreeMap<String,Integer>();
    for(String s : list){
        sorted.put(s, map.get(s));
    }
        return sorted;
}

    public static List sortByValue(final Map m) {
        List keys = new ArrayList();
        keys.addAll(m.keySet());
        Collections.sort(keys, new Comparator() {
            public int compare(Object o1, Object o2) {
                Object v1 = m.get(o1);
                Object v2 = m.get(o2);
                if (v1 == null) {
                    return (v2 == null) ? 0 : 1;
                }
                else if (v1 instanceof Comparable) {
                    return ((Comparable) v1).compareTo(v2);
                }
                else {
                    return 0;
                }
            }
        });
        return keys;
    }

}
