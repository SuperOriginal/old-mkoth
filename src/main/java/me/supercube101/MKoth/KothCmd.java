package me.supercube101.MKoth;

import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
public abstract class KothCmd {
    public abstract void execute(CommandSender sender, String[] args);
}
