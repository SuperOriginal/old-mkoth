package me.supercube101.MKoth.TimerManagement;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.MKoth;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
public class GlassTask{
    public static void run() {
        if (MKoth.i.nothingIsPlaying()) return;
        for (Koth koth : MKoth.i.getKoths()) {
            if (koth.isPlaying()) {
                if(!koth.getDead().isEmpty()){
                    for(String s : koth.getDead()){
                        Player p = Bukkit.getServer().getPlayer(s);
                        if(!(p == null) && p.getWorld().getName().equals(koth.getWorld().getName())){
                                for(Block b : koth.air){
                                    p.sendBlockChange(b.getLocation(), Material.GLASS, (byte) 0);
                            }
                        }
                    }
                }
            }
        }
    }
}
