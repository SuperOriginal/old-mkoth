package me.supercube101.MKoth.TimerManagement;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.MKoth;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
public class Cooldown {

    public static boolean sent = false;

    public static HashMap<String,KothCooldown> cooldownPlayers = new HashMap<String, KothCooldown>();

    public static void add(String player, String ability, long seconds, long systime) {
        if(!cooldownPlayers.containsKey(player)) cooldownPlayers.put(player, new KothCooldown(player));
        if(isCooling(player, ability)) return;
        cooldownPlayers.get(player).cooldownMap.put(ability, new KothCooldown(player, seconds * 1000, System.currentTimeMillis()));
    }

    public static boolean isCooling(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) return false;
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) return false;
        return true;
    }

    public static double getRemaining(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) return 0.0;
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) return 0.0;
        return utilTime.convert((cooldownPlayers.get(player).cooldownMap.get(ability).seconds + cooldownPlayers.get(player).cooldownMap.get(ability).systime) - System.currentTimeMillis(), utilTime.TimeUnit.SECONDS, 1);
    }

    public static void removeCooldown(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) {
            return;
        }
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) {
            return;
        }
        cooldownPlayers.get(player).cooldownMap.clear();
    }

    public static void handleCooldowns() {

        if(cooldownPlayers.isEmpty()) {
            return;
        }
        for(String key : cooldownPlayers.keySet()) {
            for(String name : cooldownPlayers.get(key).cooldownMap.keySet()) {
                if(MKoth.i.nothingIsPlaying()){
                    return;
                }
                for(Koth koth : MKoth.i.getKoths()){
                    if(koth.isPlaying()){
                        if(koth.getCapturer() != null) {
                            if (koth.getCapturer().getName().equalsIgnoreCase(key) && name.equalsIgnoreCase(koth.getName()) && koth.getPlayers().contains(key)) {
                                if (getRemaining(key, name) % 60 == 0 && getRemaining(key,name) != 0 && getRemaining(key, name) != 300) {

                                        if(getRemaining(key,name) == 60){
                                            if(!sent)  koth.getCapturer().sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + (int) getRemaining(key, name) / 60 + " minute left to capture.");
                                            if(!sent)  Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.BLUE + koth.getName() + " is being captured. (1:00)");
                                        }
                                        else{
                                            if(!sent)  koth.getCapturer().sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + (int) getRemaining(key, name) / 60 + " minutes left to capture.");
                                            if(!sent)  Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.BLUE + koth.getName() + " is being captured. (" + (int) getRemaining(key, name) / 60 + ":00)");
                                        }
                                        if(!sent) sent = true;
                                }
                            }
                        }
                    }
                }
                if(getRemaining(key, name) <= 0.0) {
                    for(Koth koth : MKoth.i.getKoths()){
                        if(key.equalsIgnoreCase(koth.getName())){ //This means that the KOTH expired.
                            String winner = "";
                            double highest = 0;
                            //if(!koth.getScores().isEmpty()) {
                                //for (String entry : koth.getScores().keySet()) {
                                    if (koth.getCapturer() != null) {
                                        winner = koth.getCapturer().getName();
                                    }
                               // }
                            //}

                            if(!winner.equals("")) {
                                Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + koth.getName() + " has expired! The winner is " + winner + "!");
                                if(MKoth.i.localwins.containsKey(winner)) MKoth.i.localwins.put(winner, MKoth.i.localwins.get(winner));
                                else MKoth.i.localwins.put(winner, 1);
                            }else{
                                Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + koth.getName() + " has expired with no winner.");
                            }
                            koth.reset();
                            removeCooldown(key, name);
                        }

                        for(Player p : Bukkit.getOnlinePlayers()){
                            if(key.equalsIgnoreCase(p.getName()) && name.equalsIgnoreCase(koth.getName())){ //This means a player successfully capped.
                                Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + p.getDisplayName() + ChatColor.DARK_AQUA +  " has successfully captured " + koth.getName() + "!");
                                if(MKoth.i.localwins.containsKey(p.getName())) MKoth.i.localwins.put(p.getName(), MKoth.i.localwins.get(p.getName()) + 1);
                                else MKoth.i.localwins.put(p.getName(), 1);
                                koth.reset();
                                removeCooldown(key, name);
                            }
                        }
                    }
                }
            }
        }
    }


}
