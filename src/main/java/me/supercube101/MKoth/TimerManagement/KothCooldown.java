package me.supercube101.MKoth.TimerManagement;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
public class KothCooldown {

    public HashMap<String, KothCooldown> cooldownMap = new HashMap<String,KothCooldown>();

    public String ability = "";
    public String player = "";
    public long seconds;
    public long systime;

    public KothCooldown(String player, long seconds, long systime) {
        this.player = player;
        this.seconds = seconds;
        this.systime = systime;
    }

    public KothCooldown(String player) {
        this.player = player;
    }
}
