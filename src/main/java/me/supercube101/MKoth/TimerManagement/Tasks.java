package me.supercube101.MKoth.TimerManagement;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.MKoth;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Aaron.
 */
public class Tasks {
    public void handleScores() {
        for (Koth koth : MKoth.i.getKoths()) {
            if (koth.isPlaying()) {
                if (koth.getCapturer() != null) {
                    if (koth.getScores().get(koth.getCapturer().getName()) != null)
                        koth.getScores().put(koth.getCapturer().getName(), koth.getScores().get(koth.getCapturer().getName()) + 1);
                    else koth.getScores().put(koth.getCapturer().getName(), 1.0);
                }
            }

        }
    }
}
