package me.supercube101.MKoth;

import me.supercube101.MKoth.Commands.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aaron.
 */
public class KothCmdHandler implements CommandExecutor{

    ArrayList<KothCmd> cmds = new ArrayList<KothCmd>();
    public KothCmdHandler(){
        cmds.add(new Create());
        cmds.add(new Remove());
        cmds.add(new KList());
        cmds.add(new Start());
        cmds.add(new Stop());
        cmds.add(new PurgeDatabase());
        cmds.add(new Stats());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {

        if(cmd.getName().equalsIgnoreCase("koth")){
            if(!sender.hasPermission("mkoth.admin")){
                handlePlayerMsg(sender,args);
                return true;
            }
            if(args.length == 0){
                sender.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "-------------------------------------");
                for(KothCmd kcmd : cmds){
                    CmdInfo info = kcmd.getClass().getAnnotation(CmdInfo.class);
                    sender.sendMessage(ChatColor.GRAY + " • /koth " + ChatColor.AQUA + Arrays.asList(info.aliases()).get(0) + ChatColor.GRAY + ": " + info.desc());
                }
                sender.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "-------------------------------------");
                return true;
            }

            KothCmd requested = null;

            outer : for(KothCmd kcmd : cmds){

                CmdInfo info = kcmd.getClass().getAnnotation(CmdInfo.class);

                for(String use : info.aliases()){
                    if(use.equalsIgnoreCase(args[0])){
                        requested= kcmd;
                        break outer;
                    }
                }

            }

            if(requested == null){
                sender.sendMessage(ChatColor.RED + "Unknown arguments. /koth for help.");
            }else{
                List<String> newargs = new LinkedList<String>(Arrays.asList(args));
                newargs.remove(0);
                args = newargs.toArray(new String[newargs.size()]);
                requested.execute(sender, args);
            }

        }
        return true;
    }

    public void handlePlayerMsg(CommandSender sender, String[] args){
        if(args.length == 0){
            sender.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "-------------------" + ChatColor.RESET + ChatColor.DARK_GRAY + "<< " + ChatColor.DARK_AQUA + "Arenas"+ ChatColor.DARK_GRAY + " >>" + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "------------------------");
            for(Koth k : MKoth.i.getKoths()){
                sender.sendMessage(ChatColor.DARK_GRAY + ">> " + ChatColor.DARK_AQUA + k.getName() + " (" + MKoth.i.getWorldGuardInstance().getRegionManager(k.getWorld()).getRegion(k.getCaptureid()).getMinimumPoint().getBlockX() + ", "
                        + MKoth.i.getWorldGuardInstance().getRegionManager(k.getWorld()).getRegion(k.getCaptureid()).getMinimumPoint().getBlockZ() + ")");
            }
            sender.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "-------------------" + ChatColor.RESET + ChatColor.DARK_GRAY + "<< " + ChatColor.DARK_AQUA + "Leaderboards"+ ChatColor.DARK_GRAY + " >>" + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "------------------");
            sender.sendMessage(ChatColor.GRAY + "There are different categories that each stat is categorized into, each with their own leaderboard. Categories include:");
            sender.sendMessage(ChatColor.DARK_GRAY + ">> " + ChatColor.DARK_AQUA + "Wins");
            sender.sendMessage(ChatColor.DARK_GRAY + ">> " + ChatColor.DARK_AQUA + "Kills");
            sender.sendMessage(ChatColor.DARK_GRAY + ">> " + ChatColor.DARK_AQUA + "Deaths");
            sender.sendMessage(ChatColor.DARK_GRAY + ">> " + ChatColor.DARK_AQUA + "Score (Amount of time someone has been capturing a koth in seconds)");
            sender.sendMessage(ChatColor.GRAY + "Use the command " + ChatColor.ITALIC + "/koth stats {category} " + ChatColor.RESET + ChatColor.GRAY + "to view the leaderboards." +
                    "\nYou can also use the command " + ChatColor.ITALIC + "/koth stats {Player} " + ChatColor.RESET + ChatColor.GRAY + "to view stats for a specific player.");

        }
        if(args.length > 0){
            if(args[0].equalsIgnoreCase("stats")){
                Stats stats = new Stats();
                List<String> newargs = new LinkedList<String>(Arrays.asList(args));
                newargs.remove(0);
                args = newargs.toArray(new String[newargs.size()]);
                stats.execute(sender, args);
            }else{
                sender.sendMessage(ChatColor.RED + "Invalid command. /koth for help.");
            }
        }
    }
}
