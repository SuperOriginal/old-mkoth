package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Force stop a koth arena.", aliases = {"stop"})
public class Stop extends KothCmd{
    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0){
            sender.sendMessage(ChatColor.RED + "/koth stop {name}");
            return;
        }
        String arena = args[0];
        if(MKoth.i.getKoth(arena) == null){
            sender.sendMessage(ChatColor.RED + "That arena does not exist!");
            return;
        }

        Koth koth = MKoth.i.getKoth(arena);
        if(!koth.isPlaying()){
            sender.sendMessage(ChatColor.RED + "That arena hasn't started!");
            return;
        }

        koth.reset();
        sender.sendMessage(ChatColor.GREEN + "Successfully stopped KOTH on " + koth.getName());
        Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + koth.getName() + " has been cancelled!");
    }
}
