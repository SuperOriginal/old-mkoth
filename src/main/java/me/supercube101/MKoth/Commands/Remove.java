package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Remove a koth arena.", aliases = {"remove", "delete", "r"})
public class Remove extends KothCmd {
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "/koth remove {id}");
            return;
        }
        Koth requested = null;
        for(Koth koth : MKoth.i.getKoths()){
            if(koth.getName().equalsIgnoreCase(args[0])){
                requested = koth;
                break;
            }
        }

        if(requested == null){
            sender.sendMessage(ChatColor.RED + "A Koth with that ID does not exist!");
            return;
        }

        MKoth.i.getKoths().remove(requested);
        MKoth.i.getKothData().set(requested.getName(), null);
        MKoth.i.saveKothFile();

        sender.sendMessage(ChatColor.GRAY + "Successfully removed a Koth arena!");

    }
}
