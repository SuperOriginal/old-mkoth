package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "List all Koths.", aliases = {"list", "l"})
public class KList extends KothCmd{
    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.GRAY + "All registered Koths:");
        for(Koth k : MKoth.i.getKoths()) sender.sendMessage(ChatColor.GRAY + "   - " + ChatColor.AQUA + k.getName());
    }
}
