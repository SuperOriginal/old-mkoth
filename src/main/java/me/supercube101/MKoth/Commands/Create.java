package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Create a koth arena.", aliases = {"create", "c"})
public class Create extends KothCmd{
    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length < 4){
            sender.sendMessage(ChatColor.RED + "/koth create {id} {arenaregion} {captureregion} {world}");
            return;
        }

        String name = args[0];
        String arena = args[1];
        String capture = args[2];
        World world = Bukkit.getServer().getWorld(args[3]);

        if(world == null){sender.sendMessage(ChatColor.RED + "Invalid world name."); return;}
        for(Koth koth : MKoth.i.getKoths()) if(name.equalsIgnoreCase(koth.getName())){sender.sendMessage(ChatColor.RED + "A Koth with that name already exists."); return;}
        if(MKoth.i.getWorldGuardInstance().getRegionManager(world).getRegion(arena) == null){sender.sendMessage(ChatColor.RED + "Invalid arena region."); return;}
        if(MKoth.i.getWorldGuardInstance().getRegionManager(world).getRegion(capture) == null){sender.sendMessage(ChatColor.RED + "Invalid capture region."); return;}

        MKoth.i.getKoths().add(new Koth(name, world, arena, capture));

        ConfigurationSection kothsec = MKoth.i.getKothData().createSection(name);
        kothsec.set("arenaid", arena);
        kothsec.set("captureid", capture);
        kothsec.set("world", world.getName());
        MKoth.i.saveKothFile();

        sender.sendMessage(ChatColor.GRAY + "Successfully created a Koth arena!");

    }
}
