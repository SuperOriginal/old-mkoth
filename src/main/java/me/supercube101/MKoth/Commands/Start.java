package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.Koth;
import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import me.supercube101.MKoth.TimerManagement.Cooldown;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Start a koth arena.", aliases = {"start"})
public class Start extends KothCmd{
    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0){
            sender.sendMessage(ChatColor.RED + "/koth start {name}");
            return;
        }
        String arena = args[0];
        if(MKoth.i.getKoth(arena) == null){
             sender.sendMessage(ChatColor.RED + "That arena does not exist!");
             return;
        }
        if(MKoth.i.getKoth(arena).isPlaying()){
            sender.sendMessage(ChatColor.RED + "That koth has already started!");
            return;
        }

        Koth koth = MKoth.i.getKoth(arena);
        koth.playing = true;
        sender.sendMessage(ChatColor.GREEN + "Successfully started KOTH on " + koth.getName());
        Cooldown.add(koth.getName(), "run", 3600, System.currentTimeMillis());
        //3600
        Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "KOTH" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA + "KOTH has started on " + koth.getName() + "!");
        if(koth.getCapturer() == null)
            for(Player p : Bukkit.getOnlinePlayers()){
            if(koth.isOnHill(p.getLocation())){
                koth.setCapturer(p);
            }
        }
    }
}
