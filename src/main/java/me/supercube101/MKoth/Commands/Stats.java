package me.supercube101.MKoth.Commands;


import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import net.minecraft.util.org.apache.commons.lang3.text.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Show various stats for KOTH", aliases = "stats")
public class Stats extends KothCmd{
    @Override
    public void execute(CommandSender sender, String[] args) {
        DecimalFormat format = new DecimalFormat("#.##");

        if (args.length == 0) {
            sender.sendMessage(ChatColor.GRAY + "/koth " + ChatColor.DARK_AQUA + "stats {player,kills,deaths,score,wins}");
            return;
        }
        int page = 1;
        HashMap<String,Integer> requeste = new HashMap<String, Integer>();
        if(args[0].equalsIgnoreCase("kills")) requeste = MKoth.i.localkills;
        if(args[0].equalsIgnoreCase("deaths")) requeste = MKoth.i.localdeaths;
        if(args[0].equalsIgnoreCase("score")) requeste = MKoth.i.localscore;
        if(args[0].equalsIgnoreCase("wins")) requeste = MKoth.i.localwins;
        if(args.length > 1){
            try {
                if((int)(requeste.size() / 10) + 1 >= Integer.parseInt(args[1])){
                    page = Integer.parseInt(args[1]);
                }

                else{
                    page = 1;
                }
            }catch (NumberFormatException e){
                page = 1;
            }
        }




       if(args[0].equalsIgnoreCase("kills") || args[0].equalsIgnoreCase("deaths") || args[0].equalsIgnoreCase("score") || args[0].equalsIgnoreCase("wins")) page(sender, sortByValue(requeste), page, args);
       else{
           OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);
           int kills = 0;
           int deaths = 0;
           int score = 0;
           int wins = 0;
           if(MKoth.i.localdeaths.containsKey(p.getName())) deaths = MKoth.i.localdeaths.get(p.getName());
           if(MKoth.i.localwins.containsKey(p.getName())) wins = MKoth.i.localwins.get(p.getName());
           if(MKoth.i.localkills.containsKey(p.getName())) kills = MKoth.i.localkills.get(p.getName());
           if(MKoth.i.localscore.containsKey(p.getName())) score = Integer.valueOf(format.format(MKoth.i.localscore.get(p.getName()) / 20 + 1));
           if(kills == 0 && deaths == 0 && score == 0 && wins == 0){
               sender.sendMessage(ChatColor.RED + "No data found for the specified player.");
               return;
           }
           sender.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "-----" + ChatColor.RESET + "" + ChatColor.GRAY + " KOTH Stats for " + ChatColor.DARK_AQUA + WordUtils.capitalize(args[0]) + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "-----");
           sender.sendMessage(ChatColor.DARK_AQUA + "KOTHs Captured: " + ChatColor.GRAY + wins);
           sender.sendMessage(ChatColor.DARK_AQUA + "Kills: " + ChatColor.GRAY + kills);
           sender.sendMessage(ChatColor.DARK_AQUA + "Deaths: " + ChatColor.GRAY + deaths);
           sender.sendMessage(ChatColor.DARK_AQUA + "Seconds Captured: " + ChatColor.GRAY + score);
       }

    }
    public static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

            public int compare(Map.Entry<String, Integer> m1, Map.Entry<String, Integer> m2) {
                return (m2.getValue()).compareTo(m1.getValue());
            }
        });

        Map<String, Integer> result = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }



    public void page(CommandSender sender, Map<String, Integer> data, int page, String[] args) {
        DecimalFormat format = new DecimalFormat("#.##");
        int i = 1;
        i = page > 1 ? i + (page-1)*10 : 1;
        int pageSize = 10;// Entries per page
        int startEntryIndex = page == 1 ? 0 : ((page - 1) * pageSize);
        Iterator<Map.Entry<String, Integer>> it = new ArrayList<Map.Entry<String, Integer>>(data.entrySet()).listIterator(startEntryIndex);
        String scorespec = "";
        if(args[0].equalsIgnoreCase("score")) scorespec = " by Seconds Captured";

        sender.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "-----" + ChatColor.RESET + "" + ChatColor.DARK_AQUA + " KOTH Top " + WordUtils.capitalize(args[0]) + scorespec + ChatColor.DARK_GRAY + ":" + ChatColor.GRAY + " Page "  + ChatColor.DARK_AQUA

                + page + ChatColor.GRAY + " of " + ChatColor.DARK_AQUA + ((int) (data.size() / pageSize) + 1) + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "-----");
        for (int entryIndex = startEntryIndex; entryIndex < pageSize + startEntryIndex; entryIndex++) {
            if(it.hasNext()) {

                Map.Entry<String, Integer> entry = it.next();
                String p = entry.getKey();
                int score = entry.getValue();

                if(args[0].equalsIgnoreCase("score")) score = Integer.valueOf(format.format(score / 20 + 1));
                if(p != null)sender.sendMessage(ChatColor.GRAY + " ‣ " +  + i  +  ". " + ChatColor.DARK_AQUA + WordUtils.capitalize(p) + ChatColor.DARK_GRAY + " - " +ChatColor.DARK_AQUA +  score);
                if(p != null)i++;
            }
        }
    }


}
