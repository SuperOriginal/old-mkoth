package me.supercube101.MKoth.Commands;

import me.supercube101.MKoth.KothCmd;
import me.supercube101.MKoth.MKoth;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Aaron.
 */
@CmdInfo(desc = "Clear player data in the DB", aliases = "cleardb")
public class PurgeDatabase extends KothCmd{

    @Override
    public void execute(CommandSender sender, String[] args) {
        MKoth.i.coll.drop();
        MKoth.i.localkills.clear();
        MKoth.i.localscore.clear();
        MKoth.i.localdeaths.clear();
        MKoth.i.localwins.clear();
        sender.sendMessage(ChatColor.GREEN + "Successfully cleared the database!");
    }
}
